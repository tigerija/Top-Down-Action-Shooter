﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInput : MonoBehaviour {

	public bool LMBPress = false;	// Telestun
	public bool RMBPress = false;	// Teleport
	public bool SpacePress = false;	// Dash
	public bool ShiftPress = false;	// Dash
}
