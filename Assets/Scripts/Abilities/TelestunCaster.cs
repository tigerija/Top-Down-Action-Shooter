﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelestunCaster : MonoBehaviour {

	public GameObject projectilePrefab;
	public GameObject castPositionInstance;
	GameObject _projectileInstance;

	void Update ()
	{
		if (Input.GetMouseButtonDown (0))
		{
			//Instantiate (projectileSlot);
			//projectileSlot.transform.position = barrelSlot.transform.position;
			//projectileSlot.transform.rotation = transform.rotation;

			_projectileInstance = Instantiate (projectilePrefab);
			//GameObject _barrel = castPositionInstance;
			_projectileInstance.transform.position = castPositionInstance.transform.position;
			_projectileInstance.transform.rotation = transform.rotation;
			_projectileInstance.GetComponent<TelestunProjectile> ().Init (this);
		}
	}


}
