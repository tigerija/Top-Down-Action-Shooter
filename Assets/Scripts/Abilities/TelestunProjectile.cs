﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TelestunProjectile : MonoBehaviour {

	//public CharacterFaceMouse playerFaceCursor;

	public float speed;
	public TelestunCaster telestunThatFireMe;

	void Start ()
	{
		Destroy (gameObject, 3f);
	}

	void Update ()
	{
		transform.Translate(Vector3.forward * speed * Time.deltaTime);
		if (Input.GetKeyDown ("space"))
		{ 
			telestunThatFireMe.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
			Destroy (gameObject);
		}
	}

	public void Init(TelestunCaster telestunThatFiredThisProjectile)
	{
		this.telestunThatFireMe = telestunThatFiredThisProjectile;
	}
		
}
