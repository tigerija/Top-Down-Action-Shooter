﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbAttack : MonoBehaviour {

	//public float range = 1000;

	public List<GameObject> enemiesInRange = new List<GameObject>();	// declare list for units in range
 
	private void OnTriggerEnter (Collider collidingObject)	// kad objekt uđe u collider (OnTriggerExit) spremimo ga u variablu (collidingObject), zatim dodamo objekt iz varijable (collidingObject.gameObject) na listu.
	{
		if (collidingObject.tag == "Enemy")
		{
			enemiesInRange.Add (collidingObject.gameObject);
		}
	}

	private void OnTriggerExit (Collider collidingObject)
	{
		if (collidingObject.tag == "Enemy")
		{
			enemiesInRange.Remove (collidingObject.gameObject);
		}
	}

	GameObject FindClosestEnemy ()
	{

		//Vector3 _characterPosition = transform.position;	// netreba?
		float enemyDistance = Mathf.Infinity;	// šta je ovi inifinity?
		GameObject closestEnemy = null;
		foreach (GameObject _enemy in enemiesInRange)
		{
			//float _distance = Vector3.Distance (enemyTarget.transform.position, transform.position);

			Vector3 _distanceDifference = _enemy.transform.position - transform.position/*_characterPosition*/;
			float _currentDistance = _distanceDifference.sqrMagnitude;

			if (_currentDistance < enemyDistance)
			{
				closestEnemy = _enemy;
				enemyDistance = _currentDistance;
			}

			GameObject model = _enemy.transform.GetChild(0).gameObject; 
			Renderer rend = model.GetComponent<Renderer> ();
			rend.material.color = Color.red;
		}
		return closestEnemy;
	}

	void Update ()
	{
		Debug.Log (FindClosestEnemy().name);
		Debug.Log (enemiesInRange.Count);

		GameObject closestEnemy = FindClosestEnemy ();
		if (closestEnemy != null) {
			GameObject model = closestEnemy.transform.GetChild(0).gameObject;

			Renderer rend = model.GetComponent<Renderer> ();
			rend.material.color = Color.Lerp (Color.red, Color.green, Mathf.PingPong (Time.time, 1.0f) / 1.0f);
		}

	}
}