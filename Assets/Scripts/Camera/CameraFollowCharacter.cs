﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowCharacter : MonoBehaviour {

    public Transform characterSlot;
	[HideInInspector] public Vector3 offset;

	[Range(0.01f, 10.0f)]
	public float smoothValue;

	void Start ()
	{ 
		offset = transform.position - characterSlot.position;
	}

	void LateUpdate ()
    {
		Vector3 newPosition = characterSlot.position + offset;
		transform.position = Vector3.Lerp (transform.position, newPosition, smoothValue * Time.deltaTime);
		//transform.position = Vector3.Slerp (transform.position, defaultPosition, smoothValue * Time.deltaTime);

	}
}
