﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMove : MonoBehaviour {
	
	Properties Properties;	// we declare this variable

	void Start ()
	{
		Properties = gameObject.GetComponent<Properties>();	// we get access to component by assigning it to variable, thus we make form a link to it
	}

	void Update ()
	{
		// Movement Input
		float moveHorizontal = Input.GetAxisRaw ("Horizontal");
		float moveVertical = Input.GetAxisRaw ("Vertical");
		Properties.direction = new Vector3 (moveHorizontal, 0, moveVertical).normalized; // normalize Vector3 diagonal magnitude.
		transform.position += Properties.direction * Properties.speed * Time.deltaTime;
	}
}
