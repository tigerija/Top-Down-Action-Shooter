﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLookAtMouse : MonoBehaviour {

	public LayerMask groundLayer;

	void Update ()
	{
		RotatePlayerTowardsMouse ();
	}

	public void RotatePlayerTowardsMouse()
	{
		Ray _cameraRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit _cameraRayHit;

		if (Physics.Raycast (_cameraRay, out _cameraRayHit, 100f, groundLayer)) 
		{
			Vector3 _pointToLook = _cameraRayHit.point;
			transform.LookAt (new Vector3(_pointToLook.x, transform.position.y, _pointToLook.z));
		}
	}

	public Vector3 GetPlayersDirection()
	{
		return transform.eulerAngles;
	}
}
